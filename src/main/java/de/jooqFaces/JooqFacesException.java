package de.jooqFaces;

/**
 * 
 * @author jotty
 *
 */
public class JooqFacesException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public JooqFacesException(String message) {
		super(message);
	}

	public JooqFacesException(Exception e) {
		super(e);
	}
}
