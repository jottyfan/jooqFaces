package de.jooqFaces;

import java.io.*;
import java.sql.*;
import java.util.logging.*;

import javax.sql.*;

/**
 * 
 * @author jotty
 *
 */
public class PoollessDataSource implements DataSource {

	private final String driver;
	private final String url;

	public PoollessDataSource(String driver, String url) {
		this.driver = driver;
		this.url = url;
	}

	@Override
	public PrintWriter getLogWriter() throws SQLException {
		return null;
	}

	@Override
	public int getLoginTimeout() throws SQLException {
		return 0;
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return null;
	}

	@Override
	public void setLogWriter(PrintWriter out) throws SQLException {

	}

	@Override
	public void setLoginTimeout(int seconds) throws SQLException {
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return false;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return null;
	}

	@Override
	public Connection getConnection() throws SQLException {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			throw new SQLException(e);
		}
		return DriverManager.getConnection(url);
	}

	@Override
	public Connection getConnection(String username, String password) throws SQLException {
		return null;
	}
}
