package de.jooqFaces;

/**
 * 
 * @author jotty
 *
 */
public enum EJooqFacesConnectionPool {
	CP_HIKARI("hikari");

	private final String value;
	
	private EJooqFacesConnectionPool(String value) {
		this.value = value;
	}
	
	public String get() {
		return value;
	}
	
	public static final String getHikari() {
		return CP_HIKARI.get();
	}
}
