package de.jooqFaces;

/**
 * 
 * @author henkej
 *
 */
public enum EJooqFacesApplicationScope {
	/**
	 * jooqFacesUrl
	 */
	JOOQ_FACES_URL("jooqFacesUrl"),
	/**
	 * jooqFacesDriver
	 */
	JOOQ_FACES_DRIVER("jooqFacesDriver"),
	/**
	 * jooqFacesSqldialect
	 */
	JOOQ_FACES_SQLDIALECT("jooqFacesSqldialect"), 
	/**
	 * jooqFacesProperties
	 */
	JOOQ_FACES_PROPERTIES("jooqFacesProperties"), 
	/**
	 * jooqFacesConnectionPool
	 */
	JOOQ_FACES_CONNECTIONPOOL("jooqFacesConnectionPool"),
	/**
	 * jooqFacesMaxPoolSize
	 */
	JOOQ_FACES_MAXPOOLSIZE("jooqFacesMaxPoolSize"), 
	/**
	 * jooqFacesParamAutocommit
	 */
	JOOQ_FACES_PARAM_AUTOCOMMIT("jooqFacesParamAutocommit");
	
	private final String s;

	private EJooqFacesApplicationScope(String s) {
		this.s = s;
	}

	/**
	 * @return the value
	 */
	public final String get() {
		return s;
	}
}
