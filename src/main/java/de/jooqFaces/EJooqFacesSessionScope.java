package de.jooqFaces;

/**
 * 
 * @author henkej
 *
 */
public enum EJooqFacesSessionScope {
	CONNECTION("connection");

	private final String value;

	private EJooqFacesSessionScope(String value) {
		this.value = value;
	}

	public String get() {
		return value;
	}
}
