package de.jooqFaces;

/**
 * 
 * @author henkej
 *
 */
public interface JooqFacesServletContext {

	/**
	 * Returns a String containing the value of the named context-wide initialization parameter, or null if the parameter
	 * does not exist.
	 * 
	 * This method can make available configuration information useful to an entire web application. For example, it can
	 * provide a webmaster's email address or the name of a system that holds critical data.
	 * 
	 * @param name
	 *          a String containing the name of the parameter whose value is requested
	 * @return a String containing the value of the context's initialization parameter, or null if the context's
	 *         initialization parameter does not exist.
	 * @throws NullPointerException
	 *           if the argument name is null
	 */
	public String getInitParameter(String name);

	/**
	 * Sets the context initialization parameter with the given name and value on this ServletContext.
	 *
	 * @param name
	 *          the name of the context initialization parameter to set
	 * @param value
	 *          the value of the context initialization parameter to set
	 * 
	 * @return true if the context initialization parameter with the given name and value was set successfully on this
	 *         ServletContext, and false if it was not set because this ServletContext already contains a context
	 *         initialization parameter with a matching name
	 * @throws IllegalStateException
	 *           if this ServletContext has already been initialized
	 * @throws NullPointerException
	 *           if the name parameter is null
	 * @throws UnsupportedOperationException
	 *           if this ServletContext was passed to the ServletContextListener.contextInitialized method of a
	 *           ServletContextListener that was neither declared in web.xml or web-fragment.xml, nor annotated with
	 *           jakarta.servlet.annotation.WebListener
	 */
	public boolean setInitParameter(String key, String value);

	/**
	 * Binds an object to a given attribute name in this ServletContext. If the name specified is already used for an
	 * attribute, this method will replace the attribute with the new to the new attribute.
	 * 
	 * If listeners are configured on the ServletContext the container notifies them accordingly.
	 * 
	 * If a null value is passed, the effect is the same as calling removeAttribute().
	 * 
	 * Attribute names should follow the same convention as package names. The Java Servlet API specification reserves
	 * names matching java.*, javax.*, and sun.*.
	 * 
	 * @param name
	 *          a String specifying the name of the attribute
	 * @param object
	 *          an Object representing the attribute to be bound
	 * @throws NullPointerException
	 *           if the name parameter is null
	 */
	public void setAttribute(String name, Object object);
}
