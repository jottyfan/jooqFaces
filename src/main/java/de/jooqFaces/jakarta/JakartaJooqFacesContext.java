package de.jooqFaces.jakarta;

import java.sql.*;
import java.util.*;

import jakarta.faces.application.*;
import jakarta.faces.application.FacesMessage.*;
import jakarta.faces.component.*;
import jakarta.faces.context.*;
import jakarta.faces.render.*;
import javax.sql.*;

import org.apache.logging.log4j.*;
import org.jooq.*;
import org.jooq.impl.*;

import com.zaxxer.hikari.*;

import de.jooqFaces.*;
import jakarta.servlet.*;

/**
 * 
 * @author jotty
 *
 */
public class JakartaJooqFacesContext extends FacesContext {
	private static final Logger LOGGER = LogManager.getLogger(JakartaJooqFacesContext.class);
	private FacesContext facesContext;
	private Connection connection;

	public JakartaJooqFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
		setCurrentInstance(this);
	}

	/**
	 * get the jooq dsl context from the faces context session map<br />
	 * <br />
	 * <b>Always</b> call getJooq() within a <b>try-catch closure</b>, as the DSLContext is a closure; if not, your
	 * connections might run out
	 * 
	 * @return the jooq context
	 * @throws ClassNotFoundException
	 *           on driver errors; check if you have attached the correct jdbc driver class
	 * @throws SQLException
	 *           on sql errors
	 */
	public CloseableDSLContext getJooq() throws ClassNotFoundException, SQLException {
		ExternalContext externalContext = facesContext.getExternalContext();
		if (externalContext == null) {
			throw new JooqFacesException("external context of current faces context is null");
		}
		ServletContext servletContext = (ServletContext) externalContext.getContext();
		if (servletContext == null) {
			throw new JooqFacesException("servlet context of current faces context is null");
		}
		JooqFacesServletContext ctx = new JakartaServletContext(servletContext);
		SQLDialect dialect = getSqlDialect(ctx);
		createConnectionIfNull(externalContext, ctx);
		return new DefaultCloseableDSLContext(new DefaultConnectionProvider(connection), dialect);
	}

	/**
	 * get the database connection from the session map; if not found, create a new one and add it to the session map
	 * 
	 * @param sessionMap
	 *          the session map
	 * @param externalContext
	 *          the external context
	 * @param servletContext
	 *          the servlet context
	 * @return the connection
	 * @throws ClassNotFoundException
	 *           on driver errors (e.g. missing jdbc lib)
	 * @throws SQLException
	 *           on sql errors
	 */
	private void createConnectionIfNull(ExternalContext externalContext, JooqFacesServletContext servletContext)
			throws ClassNotFoundException, SQLException {
		if (connection == null) { // caching the connection within the faces context makes it faster on the jsf life cycle
			Map<String, Object> sessionMap = externalContext.getSessionMap();
			if (sessionMap == null) {
				throw new JooqFacesException("session map of current faces context is null");
			}
			DataSource dataSource = (DataSource) sessionMap.get(EJooqFacesSessionScope.CONNECTION.get());
			if (dataSource == null || dataSource.getConnection() == null || dataSource.getConnection().isClosed()) {
				LOGGER.debug("creating new connection pool");
				dataSource = getDataSourceFromServletContext(servletContext);
				externalContext.getSessionMap().put(EJooqFacesSessionScope.CONNECTION.get(), dataSource);
			}
			connection = dataSource.getConnection();
			String autoCommit = servletContext.getInitParameter(EJooqFacesApplicationScope.JOOQ_FACES_PARAM_AUTOCOMMIT.get());
			connection.setAutoCommit("true".equals(autoCommit)); // default false for postgreSQL, the database of my choice
		}
	}

	/**
	 * get data source from connection pool if defined in servlet context (see
	 * EJooqFacesApplicationScope.CONNECTION_POOL); if not defined, return a plain data source
	 * 
	 * @param servletContext
	 * @return
	 */
	private static final DataSource getDataSourceFromServletContext(JooqFacesServletContext servletContext)
			throws ClassNotFoundException {
		String driver = getDriver(servletContext);
		if (driver == null) {
			throw new JooqFacesException(
					"undefined driver in application scope, define it in your web.xml's context-param on name "
							+ EJooqFacesApplicationScope.JOOQ_FACES_DRIVER.get());
		}
		String url = getUrl(servletContext);
		if (url == null) {
			throw new JooqFacesException(
					"undefined connection data url in application scope, define it in your web.xml's context-param on name "
							+ EJooqFacesApplicationScope.JOOQ_FACES_URL.get());
		}
		Integer maxPoolSize = getMaxPoolSize(servletContext);
		if (maxPoolSize == null) {
			LOGGER.debug("maxPoolSize not set, setting it to 20");
			maxPoolSize = 20;
		}
		String connectionPool = getConnectionPool(servletContext);
		if (connectionPool == null) {
			LOGGER.warn(
					"no connection pool set in servlet context (see EJooqFacesApplicationScope.JOOQ_FACES_CONNECTIONPOOL), using plain connection");
		}
		DataSource dataSource;
		if (EJooqFacesConnectionPool.getHikari().equals(connectionPool)) {
			dataSource = getHikariDataSource(driver, url, maxPoolSize);
		} else {
			dataSource = new PoollessDataSource(driver, url);
		}
		return dataSource;
	}

	/**
	 * get hikari data source connection pool
	 * 
	 * @param servletContext
	 *          the servlet context
	 * @return the hikari data source connection pool
	 */
	private static final HikariDataSource getHikariDataSource(String driver, String url, Integer maxPoolSize)
			throws ClassNotFoundException {
		Class.forName(driver);
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(url);
		config.addDataSourceProperty("autoCommit", false);
		config.setMaximumPoolSize(maxPoolSize);
		return new HikariDataSource(config);
	}

	/**
	 * get the connection from the servlet context
	 * 
	 * @param servletContext
	 *          the servlet context
	 * @return the connection
	 * @throws ClassNotFoundException
	 *           on driver errors (e.g. missing jdbc lib)
	 * @throws SQLException
	 *           on sql errors
	 */
	private static final Connection getConnectionFromServletContext(JooqFacesServletContext servletContext)
			throws ClassNotFoundException, SQLException {
		DataSource dataSource = getDataSourceFromServletContext(servletContext);
		return dataSource.getConnection();
	}

	/**
	 * get a jooq connection from servlet context (for such cases as the deployment phase where the faces context is still
	 * not available)
	 * 
	 * @param servletContext
	 *          the servlet context
	 * @return a jooq connection
	 * @throws ClassNotFoundException
	 *           on driver errors (e.g. missing jdbc lib)
	 * @throws SQLException
	 *           on sql errors
	 */
	public static final CloseableDSLContext getJooqFromServletContext(JooqFacesServletContext servletContext)
			throws ClassNotFoundException, SQLException {
		SQLDialect dialect = getSqlDialect(servletContext);
		Connection con = getConnectionFromServletContext(servletContext);
		return new DefaultCloseableDSLContext(new DefaultConnectionProvider(con), dialect);
	}
	
	@Override
	public void addMessage(String clientId, FacesMessage message) {
		facesContext.addMessage(clientId, message);
	}

	@Override
	public Application getApplication() {
		return facesContext.getApplication();
	}

	@Override
	public Iterator<String> getClientIdsWithMessages() {
		return facesContext.getClientIdsWithMessages();
	}

	@Override
	public ExternalContext getExternalContext() {
		return facesContext.getExternalContext();
	}

	@Override
	public Severity getMaximumSeverity() {
		return facesContext.getMaximumSeverity();
	}

	@Override
	public Iterator<FacesMessage> getMessages() {
		return facesContext.getMessages();
	}

	@Override
	public Iterator<FacesMessage> getMessages(String clientId) {
		return facesContext.getMessages(clientId);
	}

	@Override
	public RenderKit getRenderKit() {
		return facesContext.getRenderKit();
	}

	@Override
	public boolean getRenderResponse() {
		return facesContext.getRenderResponse();
	}

	@Override
	public boolean getResponseComplete() {

		return facesContext.getResponseComplete();
	}

	@Override
	public ResponseStream getResponseStream() {
		return facesContext.getResponseStream();
	}

	@Override
	public ResponseWriter getResponseWriter() {
		return facesContext.getResponseWriter();
	}

	@Override
	public UIViewRoot getViewRoot() {
		return facesContext.getViewRoot();
	}

	@Override
	public void release() {
		facesContext.release();
	}

	@Override
	public void renderResponse() {
		facesContext.renderResponse();
	}

	@Override
	public void responseComplete() {
		facesContext.responseComplete();
	}

	@Override
	public void setResponseStream(ResponseStream responseStream) {
		facesContext.setResponseStream(responseStream);
	}

	@Override
	public void setResponseWriter(ResponseWriter responseWriter) {
		facesContext.setResponseWriter(responseWriter);
	}

	@Override
	public void setViewRoot(UIViewRoot root) {
		facesContext.setViewRoot(root);
	}

	/**
	 * get the connection pool from initial context
	 * 
	 * @param servletContext
	 *          the context
	 * @return the connection pool string or null
	 */
	private static final String getConnectionPool(JooqFacesServletContext servletContext) {
		return servletContext.getInitParameter(EJooqFacesApplicationScope.JOOQ_FACES_CONNECTIONPOOL.get());
	}

	/**
	 * get the max pool size from initial context if any
	 * 
	 * @param servletContext
	 *          the context of this function call
	 * @return the max pool size or null
	 */
	private static final Integer getMaxPoolSize(JooqFacesServletContext servletContext) {
		String maxPoolSize = servletContext.getInitParameter(EJooqFacesApplicationScope.JOOQ_FACES_MAXPOOLSIZE.get());
		return maxPoolSize == null ? null : Integer.valueOf(maxPoolSize);
	}

	/**
	 * get driver from initial context
	 * 
	 * @param servletContext
	 *          the context of this function call
	 * @return the parameter value of the jooq faces driver
	 */
	private static final String getDriver(JooqFacesServletContext servletContext) {
		return servletContext.getInitParameter(EJooqFacesApplicationScope.JOOQ_FACES_DRIVER.get());
	}

	/**
	 * get driver connection url from initial context
	 * 
	 * @param servletContext
	 *          the context of this function call
	 * @return the parameter value of the jooq faces url
	 */
	private static final String getUrl(JooqFacesServletContext servletContext) {
		return servletContext.getInitParameter(EJooqFacesApplicationScope.JOOQ_FACES_URL.get());
	}

	/**
	 * find jooq sql dialect class for dialectName
	 * 
	 * @param dialectName
	 *          name of dialect
	 * @return SQLDialect if found, null otherwise
	 */
	private static final SQLDialect findDialect(String dialectName) {
		if (dialectName == null) {
			LOGGER.error("Sql dialect name is null");
			return null;
		} else {
			for (SQLDialect dialect : SQLDialect.values()) {
				LOGGER.trace("Sql dialect comparing: dialectName={}, loopDialect={}", dialectName, dialect);
				if (dialectName.equalsIgnoreCase(dialect.name())) {
					LOGGER.debug("Sql dialect found: dialectName={}, foundDialect={}", dialectName, dialect);
					return dialect;
				}
			}
			LOGGER.error("Sql dialect not found: dialectName={}", dialectName);
			return null;
		}
	}

	/**
	 * get jooq sql dialect from initial context
	 * 
	 * @param servletContext
	 *          the context of this function call
	 * @return the dialect or null
	 */
	private static final SQLDialect getSqlDialect(JooqFacesServletContext servletContext) {
		String dialectName = servletContext.getInitParameter(EJooqFacesApplicationScope.JOOQ_FACES_SQLDIALECT.get());
		return getSqlDialect(dialectName);
	}

	/**
	 * get sql dialect from name
	 * 
	 * @param name
	 *          the dialect name
	 * @return the dialect or null
	 */
	public static final SQLDialect getSqlDialect(String name) {
		return findDialect(name);
	}
}
