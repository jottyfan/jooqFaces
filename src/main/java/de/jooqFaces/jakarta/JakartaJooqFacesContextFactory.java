package de.jooqFaces.jakarta;

import jakarta.faces.*;
import jakarta.faces.context.*;
import jakarta.faces.lifecycle.*;

/**
 * 
 * @author jotty
 *
 */
public class JakartaJooqFacesContextFactory extends FacesContextFactory {

	public JakartaJooqFacesContextFactory(FacesContextFactory facesContextFactory) {
		super(facesContextFactory);
	}

	@Override
	public FacesContext getFacesContext(Object context, Object request, Object response, Lifecycle lifecycle)
			throws FacesException {
		FacesContext facesContext = getWrapped().getFacesContext(context, request, response, lifecycle);
		return new JakartaJooqFacesContext(facesContext);
	}
}
