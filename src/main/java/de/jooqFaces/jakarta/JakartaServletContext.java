package de.jooqFaces.jakarta;

import de.jooqFaces.*;
import jakarta.servlet.*;

/**
 * 
 * @author henkej
 *
 */
public class JakartaServletContext implements JooqFacesServletContext {
	private final ServletContext ctx;

	public JakartaServletContext(ServletContext ctx) {
		this.ctx = ctx;
	}

	public String getInitParameter(String name) {
		return ctx.getInitParameter(name);
	}

	@Override
	public boolean setInitParameter(String key, String value) {
		return ctx.setInitParameter(key, value);
	}

	@Override
	public void setAttribute(String name, Object object) {
		ctx.setAttribute(name, object);
	}
}
